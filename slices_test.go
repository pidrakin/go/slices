package slices

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMap(t *testing.T) {
	t.Run("string to int", func(t *testing.T) {
		strs := []string{"a", "bb"}
		mapped := Map(strs, func(s string) int { return len(s) })
		assert.Equal(t, 2, len(mapped))
		assert.Equal(t, 1, mapped[0])
		assert.Equal(t, 2, mapped[1])
	})

	t.Run("string to string", func(t *testing.T) {
		strs := []string{"a", "bb"}
		mapped := Map(strs, func(s string) string { return s + " foobar" })
		assert.Equal(t, 2, len(mapped))
		assert.Equal(t, "a foobar", mapped[0])
		assert.Equal(t, "bb foobar", mapped[1])
	})
}

func TestContains(t *testing.T) {
	strs := []string{"a", "b", "c"}
	for _, s := range strs {
		assert.True(t, Contains(strs, s))
	}
	assert.False(t, Contains(strs, "d"))
}

func TestMax(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5, 4, 3, 2, 1}
	max := Max(ints)
	assert.Equal(t, 5, max)
	assert.Equal(t, Max([]int{}), 0)
}

func TestReverse(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	reversed := Reverse(ints)
	for i, v := range reversed {
		assert.Equal(t, ints[5-1-i], v)
	}
}

func TestPrepend(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	prepended := Prepend(ints, 0)
	assert.Equal(t, []int{0, 1, 2, 3, 4, 5}, prepended)
}

func TestReduce(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	reduced := Reduce(ints, 100, func(item int, index int, array []int, result int) int {
		return result - item
	})
	assert.Equal(t, 85, reduced)
}

func TestSum(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	sum := Sum(ints)
	assert.Equal(t, 15, sum)
}

func TestFind(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	res, ok := Find(ints, func(i int) bool {
		return i == 3
	})
	assert.True(t, ok)
	assert.Equal(t, 3, res)
}

func TestRange(t *testing.T) {
	result := Range(5)
	assert.Equal(t, []int{0, 1, 2, 3, 4}, result)
}

func TestLast(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	last, err := Last(ints)
	assert.NoError(t, err)
	assert.Equal(t, 5, last)
}

func TestSort(t *testing.T) {
	ints := []int{3, 1, 2, 5, 4}
	sorted := Sort(ints)
	assert.Equal(t, []int{1, 2, 3, 4, 5}, sorted)
}

func TestFindIndex(t *testing.T) {
	ints := []int{1, 2, 3, 4, 5}
	index := FindIndex(ints, 3)
	assert.Equal(t, 2, index)
}

func TestUnpack(t *testing.T) {
	ints := []int{1, 2}
	var a, b int
	err := Unpack(ints, &a, &b)
	assert.NoError(t, err)
	assert.Equal(t, 1, a)
	assert.Equal(t, 2, b)
}

func TestUnique(t *testing.T) {
	s := []string{"a", "b", "c", "b", "a"}
	unique := Unique(s)
	assert.Equal(t, []string{"a", "b", "c"}, unique)
}
