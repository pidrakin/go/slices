package slices

import (
	"fmt"
	"sort"

	"golang.org/x/exp/constraints"
)

type StringSlice []string

type Slice[E any] []E

func Map[T any, R any](slice []T, f func(T) R) []R {
	var result []R
	for _, e := range slice {
		result = append(result, f(e))
	}
	return result
}

func Contains[T comparable](slice []T, element T) bool {
	for _, e := range slice {
		if e == element {
			return true
		}
	}
	return false
}

func Max[T constraints.Ordered](slice []T) T {
	if len(slice) == 0 {
		var zero T
		return zero
	}
	m := slice[0]
	for _, v := range slice {
		if m < v {
			m = v
		}
	}
	return m
}

func Reverse[T any](slice []T) []T {
	reversed := make([]T, len(slice))
	for i, v := range slice {
		reversed[len(slice)-1-i] = v
	}
	return reversed
}

func Prepend[T any](slice []T, element T) []T {
	return append([]T{element}, slice...)
}

func Reduce[T any, R any](slice []T, initial R, reducer func(item T, index int, slice []T, result R) R) R {
	for i, e := range slice {
		initial = reducer(e, i, slice, initial)
	}
	return initial
}

func Sum[T constraints.Ordered](slice []T) T {
	var zero T
	return Reduce(slice, zero, func(e T, i int, arr []T, r T) T {
		return r + e
	})
}

func Find[T any](slice []T, predicate func(e T) bool) (T, bool) {
	var zero T
	for _, e := range slice {
		if predicate(e) {
			return e, true
		}
	}
	return zero, false
}

func Range(num int) []int {
	slice := make([]int, num)
	for i := 0; i < num; i++ {
		slice[i] = i
	}
	return slice
}

func Last[T any](slice []T) (T, error) {
	var zero T
	if len(slice) == 0 {
		return zero, fmt.Errorf("length is 0")
	}
	return slice[len(slice)-1], nil
}

func Sort[T constraints.Ordered](slice []T) []T {
	dst := make([]T, len(slice))

	copy(dst, slice)

	sort.Slice(dst[:], func(i, j int) bool {
		return dst[i] < dst[j]
	})

	return dst
}

func FindIndex[T comparable](slice []T, item T) int {
	for i, v := range slice {
		if v == item {
			return i
		}
	}
	return -1
}

func Unpack[T any](s []T, vars ...*T) error {
	if len(s) != len(vars) {
		return fmt.Errorf("s length [%d] and vars length [%d] should be the same", len(s), len(vars))
	}
	for i, str := range s {
		*vars[i] = str
	}
	return nil
}

func Unique[T comparable](s []T) []T {
	var unique []T
	for _, v := range s {
		if !Contains(unique, v) {
			unique = append(unique, v)
		}
	}
	return unique
}

func Intersect[T comparable](a []T, b []T) []T {
	var result []T
	for _, v := range a {
		if Contains(b, v) {
			result = append(result, v)
		}
	}
	return result
}

func Diff(a []string, b []string) (added []string, removed []string) {
	for _, v := range a {
		if !Contains(b, v) {
			added = append(added, v)
		}
	}
	for _, v := range b {
		if !Contains(a, v) {
			removed = append(removed, v)
		}
	}
	return added, removed
}
